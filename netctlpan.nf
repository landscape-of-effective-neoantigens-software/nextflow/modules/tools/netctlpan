#!/usr/bin/env nextflow

process netctlpan {
// Predicts binding of peptides to any MHC molecule of known sequence using
// artificial neural networks (ANNs).
//
// input:
// output:

// require:
//   PEPTIDES_AND_ALLELES

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'netctlpan_container'
  label 'netctlpan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/netctlpan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides), path(alleles)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*fa.netctlpan.txt"), emit: c1_ctls

  script:
  """
  export TMPDIR="\${PWD}"

  /netCTLpan-1.1/bin/netCTLpan -listMHC > allowed_alleles
  for i in `cat *netmhcpan.alleles* | sed 's/,/\\n/g'`; do echo \${i}; grep "^\${i}\$" allowed_alleles | cat >> final_alleles; done
  EXPLICIT_ALLELES=\$(cat final_alleles | sed -z 's/\\n/\\t/g ' | sed 's/\\t\$//g')

  PEPTIDES_FILENAME="${peptides}"

  grep -v '^;' ${peptides} > peptides.no_comms.fa

  grep -n -B 1 "\\*" peptides.no_comms.fa | sed -n 's/^\\([0-9]\\{1,\\}\\).*/\\1d/p' | sed -f - peptides.no_comms.fa > peptides.no_comms.no_stops.fa

  if [[ -s ${peptides} ]]; then
    for allele in \${EXPLICIT_ALLELES}; do
      for i in 8 9 10 11; do
        /netCTLpan-1.1/bin/netCTLpan -a \${allele} -l \${i} -f peptides.no_comms.no_stops.fa >> \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netctlpan.txt&
      done
    done
  else
    echo "NO PEPTIDES" > \${PEPTIDES_FILENAME%.aa.fa}.netctlpan.txt
  fi

  wait

  for allele in \${EXPLICIT_ALLELES}; do
    cat \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netctlpan.txt >> \${PEPTIDES_FILENAME%.aa.fa}.netctlpan.txt
  done
  """
}
